#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import cv2
import numpy as np
import math

def wyswietl(img):
    cv2.imshow('image', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

# odcienie szarości
img1_org = cv2.imread('LENA_512.jpg')
img1 = cv2.cvtColor(img1_org, cv2.COLOR_RGB2GRAY)
wyswietl(img1)

img2_org = cv2.imread('statek_640_505.jpg')
img2 = cv2.cvtColor(img2_org, cv2.COLOR_RGB2GRAY)
wyswietl(img2)

# negatyw
img1 = 255 - img1_org
wyswietl(img1)

img2 = 255 - img2_org
wyswietl(img2)

# sepia
rows, cols, _ = img1_org.shape
w = 30
for r in range(rows):
    for c in range(cols):
        t = img1_org[c, r]
        y = (0.299 * t[2]) + (0.587 * t[1]) + (0.114 * t[0])
        rs = y + 2 * w
        if rs >= 255: rs = 255
        gs = y + w
        if gs >= 255: gs = 255
        img1[c, r] = [y, gs, rs]
wyswietl(img1)

# obrót 30
rows, cols, _ = img1_org.shape
M = cv2.getRotationMatrix2D((cols / 2, rows / 2), 30, 1)
img1 = cv2.warpAffine(img1_org, M, (cols, rows))
wyswietl(img1)

# obrót 45
rows, cols, _ = img1_org.shape
M = cv2.getRotationMatrix2D((cols / 2, rows / 2), 45, 1)
img1 = cv2.warpAffine(img1_org, M, (cols, rows))
wyswietl(img1)

# obrót 90
rows, cols, _ = img1_org.shape
M = cv2.getRotationMatrix2D((cols / 2, rows / 2), 90, 1)
img1 = cv2.warpAffine(img1_org, M, (cols, rows))
wyswietl(img1)

# przesunięty
rows, cols, _ = img1_org.shape
move = 100
for r in range(rows):
    for c in range(cols):
        img1[c, r] = img1_org[c, (r + move) % rows]
wyswietl(img1)

# poziomy jasności
def j(newb):
    b = 8
    delta = 2 ** (b - newb)
    lut = np.array([(math.floor(max((i - delta/2 - 1)/delta, 0) * delta + (delta/2 - 1))) / 255 for i in np.arange(0, 256)])
    img3_org =  cv2.imread('WIR_360.jpg')
    img3 = cv2.LUT(img3_org, lut)
    wyswietl(img3)
j(1)
j(2)
j(3)
j(4)
j(5)
j(8)
