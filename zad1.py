#!/usr/bin/env python2

import cv2

img = cv2.imread('LENA_512.jpg')

cv2.imshow('image', img)
cv2.waitKey(0)
cv2.destroyAllWindows()

for i in range(100):
    img[i, i] = [0, 0, 0]

cv2.imshow('image', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
